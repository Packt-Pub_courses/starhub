require 'test_helper'

describe IssuesController do
  before do
    @product = products(:duke)
    @issue = issues(:two)
    sign_in customers(:sarah)
  end

  describe "Issue read methods" do
    it "gets index" do
      get product_issues_url( @product)
      must_respond_with :success
    end

    it "gets new issue" do
      get new_product_issue_url( @product )
      must_respond_with :success
    end

    it "show issue" do
      get product_issue_url( @issue.product, @issue )
      must_respond_with :success
    end

    it "edits issue" do
      get edit_product_issue_url( @issue.product, @issue )
      must_respond_with :success
    end
  end

  describe "Issue write methods" do
    it "updates issue" do
      updated_issue = {
        comment: "This is an updated comment"
      }
      patch product_issue_url(@product, @issue), params: { issue: updated_issue }
      must_redirect_to product_issue_url(@product, @issue)
    end

    it "creates issue" do
      new_issue = {
        severity: 'MINOR',
        comment: "This is a test comment"
      }
      expect(lambda do
               post product_issues_url( @product), params: { issue: new_issue }
      end).must_change "Issue.count"

      must_redirect_to product_issue_url(@product, @product.issues.last)
    end

    it "destroys issue" do
      expect(lambda do
               delete product_issue_url(@issue.product, @issue)
      end).must_change('Issue.count', -1)

      must_redirect_to product_issues_url
    end
  end

end
