Rails.application.routes.draw do
  post 'messages/create'

  get 'messages/new'

  get 'messages/index'

  get 'home/index'
  devise_for :customers, controllers: {registrations: 'registrations'}

  root to: "home#index"
  resources :products do
    resources :issues
    resources :reviews
  end
end
