class IssuesController < ApplicationController
  before_action :set_product

   # GET /products/:product_id/issues
  # GET /products/:product_id/issues.json
  def index
    @issues = @product.issues
  end

  # GET /products/:product_id/issues/:id
  # GET /issues/:id.json
  def show
    @issue = @product.issues.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render :show, status: :ok, location: @issue }
    end
  end


  # GET /products/:product_id/issues/new
  # GET /products/:product_id/issues/new.json
  def new
    @issue = @product.issues.build

    respond_to do |format|
      format.html # new.html.erb
      format.json  { render json: @issue }
    end

  end

   # GET /products/:product_id/issues/:id/edit
  def edit
    @issue = @product.issues.find(params[:id])    
  end

  # POST /products/:product_id/issues
  # POST /products/:product_id/issues.json
  def create
    create_params = issue_params.merge({customer_id: current_customer.id})
    @issue = @product.issues.create(create_params)

    respond_to do |format|
      if @issue.save
        format.html { redirect_to([@issue.product, @issue], :notice => 'Issue was successfully created.') }
        # the location key value is an array in order to build the correct route to the issue nested resource
        format.json { render json: @issue, status: :created, location: [@issue.product, @issue] }
      else
        format.html { render :new }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end


  # PUT /products/:product_id/issues/:id
  # PUT /products/:product_id/issues/:id.json
  def update
    @issue = @product.issues.find(params[:id])

    respond_to do |format|
    update_params = issue_params.merge({customer_id: current_customer.id})
      if @issue.update_attributes(update_params)
        format.html { redirect_to([@issue.product, @issue], :notice => 'Issue was successfully updated.') }
        format.json { render :show, status: :ok, location: @issue }
      else
        format.html { render :edit }
        format.json { render json: @issue.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/:product_id/issues/:id
  # DELETE /products/:product_id/issues/:id.json
  def destroy
   
    @issue = @product.issues.find(params[:id])
    @issue.destroy

    respond_to do |format|
      #1st argument references the path /posts/:post_id/comments/
      format.html { redirect_to product_issues_url, notice: 'Issue was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
       @product = Product.find(params[:product_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def issue_params
      params.require(:issue).permit(:severity, :comment)
    end
end
